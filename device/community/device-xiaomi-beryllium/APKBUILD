# Maintainer: Joel Selvaraj <joelselvaraj.oss@gmail.com>

# Reference: <https://postmarketos.org/devicepkg>
pkgname=device-xiaomi-beryllium
pkgdesc="Xiaomi Poco F1"
pkgver=9
pkgrel=0
url="https://postmarketos.org"
license="MIT"
arch="aarch64"
options="!check !archcheck"
depends="
	postmarketos-base
	mkbootimg
	soc-qcom-sdm845
	soc-qcom-sdm845-ucm
	postmarketos-update-kernel
"
makedepends="devicepkg-dev"
subpackages="
	$pkgname-nonfree-firmware:nonfree_firmware
	$pkgname-kernel-tianma:kernel_tianma
	$pkgname-kernel-ebbg:kernel_ebbg
"

source="
	deviceinfo
	q6voiced.conf
"

build() {
	devicepkg_build $startdir $pkgname
}

package() {
	devicepkg_package $startdir $pkgname
}

nonfree_firmware() {
	pkgdesc="GPU, venus, modem firmware"
	depends="firmware-xiaomi-beryllium soc-qcom-sdm845-nonfree-firmware
		 soc-qcom-sdm845-modem"
	mkdir "$subpkgdir"

	install -Dm644 q6voiced.conf "$subpkgdir"/etc/conf.d/q6voiced
}

kernel_tianma() {
	pkgdesc="Tianma Panel. To know which panel your device use and the status of the port, Visit the Poco F1 wiki page: https://wiki.postmarketos.org/wiki/Xiaomi_Poco_F1_(xiaomi-beryllium)"
	depends="linux-postmarketos-qcom-sdm845"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

kernel_ebbg() {
	pkgdesc="EBBG Panel. To know which panel your device use and the status of the port, Visit the Poco F1 wiki page: https://wiki.postmarketos.org/wiki/Xiaomi_Poco_F1_(xiaomi-beryllium)"
	depends="linux-postmarketos-qcom-sdm845"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

sha512sums="
9a0048d9ee571e821ccaf190942e7db900ae0f636dd5d30bcf4211ed57f25988adc017ef353c205d55f73d75aa48b6f67b59d84b56a6a00448a94ae137bdb00a  deviceinfo
3a4a9322839d4b3ef9d79668a37840a9f444954759ae3c512e694051d2f9a2573db42ad6c4c1a5c75eeb861232a27ba1a8cef9b503decd54ead25a96e3dd6f98  q6voiced.conf
"
